

print('Hello world')
from database_psql.connection import Connection

database_config = ('dbname={dbname} user={user} password={password} host={host} port={port}'.format(
    dbname='animetest',
    user='postgres',
    password='postgres',
    host='127.0.0.1',
    port=5432
))

connection = Connection(database_config)

query_axample = """ INSERT INTO anime_series (id, name, description, gender, author, num_caps)
                VALUES(1, 'GTO: Great Teacher Onizuka', 'A stoory of a conflict teacher', 'comedy', 'IDK', '43')"""

connection.execute_modif_database_query(query_axample)

print(connection.execute_read_query("""select * from anime_series"""))
