import psycopg2

connection = None

class Connection:
    """ Create database connection """
    def __init__(self, config_database):
        self.conn = connection
        self.cursor = None
        try:
            if not self.conn:
                self.conn = psycopg2.connect(config_database)
                self.cursor = self.conn.cursor()
                print('[DATABASE] Connected')
        except (Exception, psycopg2.DatabaseError) as error:
            print('[ERROR] ->', error)

    def getConnection(self):
        """ Get database conection and cursor """
        return self.conn, self.cursor

    def execute_modif_database_query(self, query):
        """ Function than update database (insert and update and delete)"""
        if query and self.cursor:
            query += ';'
            self.cursor.execute(query)
            self.conn.commit()
            return True
        return False

    def execute_read_query(self, query):
        """ Function than query for Read (Select)"""
        if query and self.cursor:
            query += ';'
            self.cursor.execute(query)
            all_rows = self.cursor.fetchall()
            return all_rows
        return False



